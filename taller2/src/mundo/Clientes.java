package mundo;

import java.util.Date;

	public class Clientes extends Trabajo 
	{
	private String negocio;
	
	public Clientes(Date pFecha, String pLugar, TipoEventoTrabajo pTipo,
				String pNegocio, boolean pObligatorio, boolean pFormal) {
		
			super(pFecha, pLugar, pTipo, pObligatorio, pFormal);
			negocio=pNegocio;
			
	}
	

	public String conv (Boolean b)
	{
		if (!b) return "No";
		else
			return "Si";
	}

	public String toString()
	{
		return "Evento con CLIENTES: \n"
				+"FECHA: " + this.fecha
				+"\nLUGAR: " + this.lugar
				+"\nNEGOCIO: "+this.negocio
				+"\nTIPO EVENTO: " + this.tipo
				+"\nOBLIGATORIO: " + conv(this.obligatorio)
				+"\nFORMAL: "+ conv(this.formal);
	}


}

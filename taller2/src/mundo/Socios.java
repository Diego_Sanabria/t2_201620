package mundo;

import java.util.Date;

public class Socios extends Trabajo {

	private String empresa;
	

	public Socios(Date pFecha, String pLugar, TipoEventoTrabajo pTipo,
			String pEmpresa, boolean pObligatorio, boolean pFormal) {
		super(pFecha, pLugar, pTipo, pObligatorio, pFormal);
		empresa=pEmpresa;
	}

	public String conv (Boolean b)
	{
		if (!b) return "No";
		else
			return "Si";
	}

	public String toString()
	{
		return "Evento con SOCIOS: \n"
				+"FECHA: " + this.fecha
				+"\nLUGAR: " + this.lugar
				+"\nEMPRESA: "+this.empresa
				+"\nTIPO EVENTO: " + this.tipo
				+"\nOBLIGATORIO: " + conv(this.obligatorio)
				+"\nFORMAL: "+ conv(this.formal);
	}
}
